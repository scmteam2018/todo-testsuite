package com.mitrais.scm.todo.common;

/**
 * Created by Indrap on 12/10/2018.
 */
public class Constants {

    public static final String TODO_URL ="http://localhost:3000/#/";

    public static final String CHROME_WEB_DRIVER ="webdriver.chrome.driver";
    public static final String CHROME_WEB_DRIVER_LOCATION ="driver/chromedriver.exe";
}
