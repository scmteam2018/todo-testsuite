package com.mitrais.scm.todo.scenario;

import com.mitrais.scm.todo.common.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

/**
 * Created by Indrap on 8/10/2018.
 */
public class DeleteTodoItem {

    private WebDriver drive;
    public String url = Constants.TODO_URL;


    @BeforeTest
    public void setUp(){
        System.setProperty(Constants.CHROME_WEB_DRIVER,Constants.CHROME_WEB_DRIVER_LOCATION);
        this.drive = new ChromeDriver();
        this.drive.get(url);
    }

    @Test
    public void deleteTodoItem(){


        WebElement todoEntry = this.drive.findElement(By.id("todoEntry"));
        //add first todo
        int initTodoSize = this.drive.findElements(By.cssSelector(".todo-list li")).size();
        todoEntry.sendKeys("Visit Home No." + (new Random()).nextInt(100)+1);
        todoEntry.sendKeys(Keys.ENTER);

        todoEntry.sendKeys("Visit Home No." + (new Random()).nextInt(100)+1);
        todoEntry.sendKeys(Keys.ENTER);


        //check todoList
        WebElement todoList = this.drive.findElement(By.className("todo-list"));
        AssertJUnit.assertNotNull(todoList);

        //count todo
        int todoSize = this.drive.findElements(By.cssSelector(".todo-list li")).size();
        AssertJUnit.assertEquals(initTodoSize+2,todoSize);

        //delete all item
        List<WebElement> todoDelete = this.drive.findElements(By.cssSelector(".todo-list .destroy"));
        todoDelete.stream().forEach( item -> {
            item.click();
        });

        //count todo after delete
        int todoSizeAfterDelete = this.drive.findElements(By.cssSelector(".todo-list li")).size();
        AssertJUnit.assertEquals(0,todoSizeAfterDelete);
    }

    @AfterTest
    public void cleanUp(){
        this.drive.close();
    }

}
